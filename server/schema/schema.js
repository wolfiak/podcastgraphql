const rootQuery = require("./RootQuery");
const graphql = require("graphql");
const { GraphQLSchema } = graphql;
const Mutation = require('./Mutation');

module.exports = new GraphQLSchema({
  query: rootQuery.rootQuery,
  mutation: Mutation.mutation
});
