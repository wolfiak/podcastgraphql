const grahql = require("graphql");
const { GraphQLObjectType, GraphQLID, GraphQLString, GraphQLInt } = grahql;

const EpisodeType = new GraphQLObjectType({
  name: "Episode",
  fields: () => ({
    id: {type: GraphQLID},
    name: {
      type: GraphQLString,
      resolve: data => {
        return data.title;
      }
    },
    link: {
      type: GraphQLString,
      resolve: data => {
        return data.link;
      }
    }
  })
});

module.exports = {
  episodeType: EpisodeType
};
