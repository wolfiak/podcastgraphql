const grahql = require("graphql");
const { GraphQLObjectType, GraphQLID, GraphQLString, GraphQLList } = grahql;
const EpisodeType = require("./episodeType");

const PodcastType = new GraphQLObjectType({
  name: "Podcast",
  fields: () => ({
    id: { type: GraphQLID },
    name: { type: GraphQLString,
        resolve: data => {
            return data.name;
        }
    },
    episodes: {
      type: new GraphQLList(EpisodeType.episodeType),
      resolve: data => {
        console.log('data: ', data);
        return data.episodes;
      }
    }
  })
});

module.exports = {
  podcast: PodcastType
};
