const graphql = require('graphql');
const PodcastType = require('../types/podcastType');
const fetch = require('node-fetch');
const util = require('util');
const parseXml = util.promisify(require('xml2js').parseString);
const { GraphQLString } = graphql;
const Podcast = require('../../model/podcast');
const Episode = require('../../model/episode');

const addPodcast = {
  type: PodcastType.podcast,
  args: {
    url: { type: GraphQLString }
  },
  resolve(parent, args) {
    let podcast;
    let episodes = [];
    let lastUpdate;
    console.log('args: ', args.url);
    return fetch(args.url)
      .then(res => {
        res.headers.forEach((value, name) => {
          if (name === 'last-modified') {
            lastUpdate = value;
          }
        });
        return res.text();
      })
      .then(parseXml)
      .then(xml => {
        xml.rss.channel[0].item.forEach(item => {
          // console.log('itme: ',item);
          episodes.push(
            new Episode({
              title: item.title[0],
              link: item.guid[0],
              pubDate: item.pubDate[0]
            })
          );
        });
        // console.log('episode: ', episodes);
        podcast = new Podcast({
          name: xml.rss.channel[0].title[0],
          lastUpdate: lastUpdate,
          episodes: episodes
        });
        return podcast.save();
      });
  }
};

module.exports = {
  addPodcast: addPodcast
};
