const graphql = require('graphql');
const fetch = require('node-fetch');
const PodcastType = require('../types/podcastType');
const Podcast = require('../../model/podcast');
const { GraphQLString } = graphql;
const getLastModified = require('../utils');

const updatePodcast = {
  type: PodcastType.podcast,
  args: {
    id: { type: GraphQLString },
    url: { type: GraphQLString }
  },
  resolve(parent, args) {
    Podcast.findById(args.id).then(podcast => {
      fetch(args.url).then(nPodcast => {
       const fatchedDate = new Date(getLastModified(nPodcast));
       const storedDate = new Date(podcast.lastUpdate);
       console.log(fatchedDate, storedDate);
      });
    });
  }
};

module.exports = {
  updatePodcast: updatePodcast
};
