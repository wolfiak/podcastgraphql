const getLastModified = (res) => {
    res.headers.forEach((value, name) => {
        if (name === 'last-modified') {
         return value;
        }
      });
}

module.exports = {
    getLastModified
}