const graphql = require("graphql");
const addPodcast = require("./mutations/podcast");
const updatePodcast = require("./mutations/updatePodcast");
const { GraphQLObjectType } = graphql;

const Mutation = new GraphQLObjectType({
  name: "Mutation",
  fields: {
    addPodcast: addPodcast.addPodcast,
    updatePodcast: updatePodcast.updatePodcast
  }
});

module.exports = {
  mutation: Mutation
};
