const graphql = require("graphql");
const { GraphQLObjectType, GraphQLString } = graphql;
const PodcastType = require("./types/podcastType");
const Podcast = require("../model/podcast");

const RootQuery = new GraphQLObjectType({
  name: "RootQuery",
  fields: {
    podcast: {
      type: PodcastType.podcast,
      args: {
        name: { type: GraphQLString }
      },
      resolve: (root, args) => {
         return Podcast.findOne({name: args.name}, {episodes: {$slice: [0, 10]}});
      }
    }
  }
});

module.exports = {
  rootQuery: RootQuery
};
