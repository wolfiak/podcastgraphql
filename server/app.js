const express = require("express");
const graphglHTTP = require("express-graphql");
const mongoose = require("mongoose");
const schema = require("./schema/schema");
const cors = require('cors');

const app = express();

app.use(cors());

mongoose.connect(
  "mongodb://pariko:haslo1@ds113482.mlab.com:13482/podcast",
  {
    useNewUrlParser: true
  }
);

mongoose.connection.once("open", () => {
  console.log("Contected to database :)");
});

app.use(
  "/graphql",
  graphglHTTP({
    schema,
    graphiql: true
  })
);

app.listen(4000, () => {
  console.log("Server started at port 4000...");
});
