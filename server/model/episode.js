const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const episodeSchema = new Schema({
  title: String,
  link: String,
  pubDate: String,
  // podcast: { type: Schema.Types.ObjectId, ref: "Podcast" }
});

module.exports = mongoose.model("Episode", episodeSchema);
