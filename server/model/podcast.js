const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const podcastSchema = new Schema({
  name: String,
  lastUpdate: String,
  episodes: [
    {
      title: String,
      link: String,
      pubDate: String
    }
  ]
});

module.exports = mongoose.model('Podcast', podcastSchema);
