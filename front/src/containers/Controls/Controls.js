import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import './Controls.css';
import { Howl } from 'howler';

class Controls extends Component {
  sound = null;
  timer = null;
  seek = null;

  constructor(props) {
    super(props);
    this.state = {
      seek: '00:00:00',
      played: null,
      duration: '00:00:00',
      seekPos: 0,
      durationNumeric: 0,
      soundValue: 0
    };
  }

  playEpisode() {
    this.sound = new Howl({
      src: [this.props.currentEpisode.link],
      html5: true
    });
    this.sound.play();
    this.sound.on('load', () => {
      const duration = this.sound.duration();
      this.setState({
        durationNumeric: Math.floor(duration),
        duration: this.getTimeInDisplayFormat(duration),
        soundValue: Math.floor((this.sound.volume() * 100) | 0)
      });
    });
    this.setState({ played: this.props.currentEpisode });
    this.timer = setInterval(this.durationUpadte.bind(this), 1000);
  }

  addZeroAfterNumber(time) {
    return time > 9 ? `${time}` : `0${time}`;
  }

  componentDidUpdate() {
    if (Object.keys(this.props.currentEpisode).length !== 0) {
      if (this.props.play) {
        if (!this.sound) {
          this.playEpisode();
        } else if (
          this.state.played &&
          this.state.played.id !== this.props.currentEpisode.id
        ) {
          this.playEpisode();
        }
      } else {
        if (this.sound) {
          clearInterval(this.timer);
          this.sound.stop();
        }
      }
    }
  }

  getTimeInDisplayFormat(sec) {
    let seconds = isNaN(sec) ? 0 : Math.floor(sec);
    const hours = Math.floor(seconds / (60 * 60));
    seconds -= hours * (60 * 60);
    const minutes = Math.floor(seconds / 60);
    seconds -= minutes * 60;

    return `${this.addZeroAfterNumber(hours)}:${this.addZeroAfterNumber(
      minutes
    )}:${this.addZeroAfterNumber(seconds)}`;
  }

  durationUpadte() {
    if (this.sound) {
      const _seek = this.sound.seek();
      this.setState({
        seekPos: Math.floor((_seek / this.state.durationNumeric) * 100) || 0,
        seek: this.getTimeInDisplayFormat(this.sound.seek())
      });
    }
  }

  stopEpisode() {
    this.sound.stop();
    this.props.playStateChnage();
  }

  changeSound(event) {
    this.setState({ soundValue: event.target.value });
    this.sound.volume(event.target.value / 100);
  }

  render() {
    return (
      <div className="controls__wrapper">
        <div>
          <p>{this.props.currentEpisode.name}</p>
        </div>
        <div className="controls__content">
          <div>
            <FontAwesomeIcon icon="step-backward" />
          </div>
          <div>
            {this.props.play ? (
              <FontAwesomeIcon
                onClick={this.stopEpisode.bind(this)}
                icon="stop"
              />
            ) : (
              <FontAwesomeIcon icon="play" />
            )}
          </div>
          <div>
            <FontAwesomeIcon icon="step-forward" />
          </div>
        </div>
        <div className="controls__volume-bar__container">
          <div className="controls__relative">
            <FontAwesomeIcon icon="volume-up" />
            <input
              className="controls__volumea-bar"
              value={this.state.soundValue}
              onChange={this.changeSound.bind(this)}
              min="0"
              step="10"
              max="100"
              type="range"
            />
          </div>
        </div>
        <div className="controls__track">
          <div className="controls__time-displayed">
            <div>{this.state.seek}</div>
            <div>{this.state.duration}</div>
          </div>
          <input
            type="range"
            value={this.state.seekPos}
            min="0"
            max="100"
            step="1"
          />
        </div>
      </div>
    );
  }
}

export default Controls;
