import React, { Component } from 'react';
import PodcastProvider from './provider';
import Layout from './components/Layout/Layout';
import { ApolloProvider } from 'react-apollo';
import ApolloClient from 'apollo-boost';
import { library } from '@fortawesome/fontawesome-svg-core';

import { faPlay, faStepForward, faStepBackward, faStop, faVolumeUp } from '@fortawesome/free-solid-svg-icons';

library.add(faPlay, faStepForward, faStepBackward, faStop, faVolumeUp);

const client = new ApolloClient({
  uri: "http://localhost:4000/graphql"
});
class App extends Component {
  render() {
    return (
      <div>
        <ApolloProvider client={client}>
          <PodcastProvider>
            <Layout />
          </PodcastProvider>
        </ApolloProvider>
      </div>
    );
  }
}

export default App;
