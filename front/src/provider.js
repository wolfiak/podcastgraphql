import React, { Component } from 'react';
import { getPodcast } from './quieries';
import { graphql, compose } from 'react-apollo';
export const PodcastContext = React.createContext();

class PodcastProvider extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: 'Podcast',
      play: false,
      episodes: [],
      currentEpisode: {}
    };
  }

  static getDerivedStateFromProps(props, state) {
    console.log('propsy: ',props.getPodcast);
    if (props.getPodcast.loading || !!props.getPodcast.error) {
      return null;
    } else {
      console.log('elsE: ')
      return {
        ...state,
        episodes: props.getPodcast.podcast.episodes
      };
    }
  }

  render() {
    return (
      <PodcastContext.Provider
        value={{
          state: this.state,
          currentEpisode: { ...this.state.currentEpisode },
          play: this.state.play,
          passEpisodeToPlay: episode => {
            this.setState({ play: true, currentEpisode: { ...episode } });
          },
          playStateChnage: () => {
            this.setState((prev)=> {
              console.log('prev: ', prev);
             return {play: !prev.play}
            });
            console.log('play state: ', this.state)
          }
        }}
      >
        {this.props.children}
      </PodcastContext.Provider>
    );
  }
}

export default compose(graphql(getPodcast, { name: 'getPodcast' }))(
  PodcastProvider
);
