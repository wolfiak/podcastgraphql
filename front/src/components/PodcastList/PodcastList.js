import './PodcastList';
import React, { Component } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

class PodcastList extends Component {
  render() {
    let list = null;
    if (this.props.episodes.length === 0) {
      list = <h2>LOADING...</h2>;
    } else {
      list = (
        <ul>
          {this.props.episodes.map(podcast => {
            return (
              <li key={podcast.id}>
                {podcast.name}
                <button onClick={() => this.props.play ? this.props.playStateChnage() : this.props.passEpisode(podcast)}>
                  { (this.props.currentEpisode.id === podcast.id) && this.props.play ? <FontAwesomeIcon  icon="stop"/> : <FontAwesomeIcon icon="play"/>}
                </button>
              </li>
            );
          })}
        </ul>
      );
    }
    return <React.Fragment>{list}</React.Fragment>;
  }
}

export default PodcastList;
