import React from 'react';
import Navbar from '../Navbar/Navbar';
import './Layout.css';
import { PodcastContext } from '../../provider';
import Controls from '../../containers/Controls/Controls';
import PodcastList from '../PodcastList/PodcastList';

const Layout = () => (
  <React.Fragment>
    <Navbar />
    <PodcastContext.Consumer>
      {context => (
        <React.Fragment>
          <div className="layout__content">
            <PodcastList
              episodes={context.state.episodes}
              passEpisode={context.passEpisodeToPlay}
              currentEpisode={context.currentEpisode}
              playStateChnage={context.playStateChnage}
              play={context.play}
            />
            Test {JSON.stringify(context)}
          </div>
          <Controls
            play={context.play}
            playStateChnage={context.playStateChnage}
            currentEpisode={context.currentEpisode}
          />
        </React.Fragment>
      )}
    </PodcastContext.Consumer>
  </React.Fragment>
);

export default Layout;
