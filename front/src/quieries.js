import { gql } from 'apollo-boost';

export const getPodcast = gql`
  {
    podcast(name: "Rozgrywka") {
      name,
      episodes{
        name
        id
        link
      }
    }
  }
`;
